# Yet Another Virtual Machine
Just like everyone else on the hopson community discord, I have started a virtual CPU project.

## Brainfuck*X
Since I was too lazy to make my own assembler, here is the language I am using currently (I will write a compiler for my CPU based on the specification):
```
--== BF*X (Brainfuck times x) specs ==--

Enhancements:
# 					prints current byte as number
+x 					adds x number onto cell
-x					subtracts x number from cell
>x					moves pointer by x to the right
<x					moves pointer by x to the left
/x					divides data in cell by x
*x					multiplies data in cell by x
&					data in current cell (in place of x)
rx					Runs X amount of bytes from memory
@r					(Where r is a register)
					stores current byte in register r
gx					(where x is position)
					go to a pos and continue running program

filesystem (fs):
\x					(X is a regular opperator minus letter ops)
					acts like the fs is memory.
\Lx					Load X amount of bytes from fs into memory

Variable ops:
:s"x"				Creates string from "x" and put every byte
					into a cell (starting from current)
```